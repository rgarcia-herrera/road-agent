from LatLon23 import LatLon, Latitude, Longitude
from road_agent import Agent


def test_update_route():
    point = LatLon(Latitude(19.40141579973),
                   Longitude(-99.1043955014))

    dest = LatLon(Latitude(19.44658420026),
                  Longitude(-99.15200449858))

    b = Agent(point=point, dest=dest)
    b.update_route()
    assert len(b.route) == 3284
