from LatLon23 import LatLon, Latitude, Longitude
from road_agent.router import NXRouter
import osmnx as ox

#def test_get_route():

points = [LatLon(19.3881769, -99.1794493),
          LatLon(19.3858363, -99.1767216)]

G = ox.graph_from_point((19.3838,-99.1758), distance=1000)

router = NXRouter(G)
r = router.get_raw_route(points=points)
