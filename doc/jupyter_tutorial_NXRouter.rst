=================================
Example simulation using NXRouter
=================================

This is the simpler of the two tutorials for it does not require a running BRouter server.

Jupyter interactive tutorial
____________________________

You can download the following notebook from
https://gitlab.com/rgarcia-herrera/road-agent/blob/master/doc/jupyter_tutorial_NXRouter.ipynb
to run a local copy and do some experiments.

Try changing speeds for modes, initial population sizes. Try other maps. Enjoy!


.. raw:: html
   :file: jupyter_tutorial_NXRouter.html


