============
Installation
============

Install it using pip, perhaps within a virtualenv. Like so::

  pip install road-agent

It should install all python library requirements.



Install dependency: BRouter
___________________________


Agents use `BRouter`__ for routing. Here's how to install a local server.

.. __: http://brouter.de/brouter/

BRouters needs data files for its routing algorithm. Download `data
files`__ (rd5) into segments4 dir. You may copy-paste these steps to
download them all, but beware: it's about 4.5 gigabytes.
::

  mkdir -p ~/opt/brouter/segments4
  cd ~/opt/brouter/segments4
  wget https://gitlab.com/rgarcia-herrera/road-agent/raw/master/get_segments.sh
  wget https://gitlab.com/rgarcia-herrera/road-agent/raw/master/segments.txt
  chmod +x get_segments.sh
  ./get_segments.sh


.. __: http://brouter.de/brouter/segments4/


Download and unzip BRouter (replace ~/opt/ with your preferred install
dir and 1_4_11 with latest version)::

  cd ~/opt/brouter
  wget http://brouter.de/brouter_bin/brouter_1_4_11.zip
  unzip brouter_1_4_11.zip
  chmod +x ./standalone/server.sh
