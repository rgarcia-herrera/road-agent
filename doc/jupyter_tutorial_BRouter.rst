
=======================================
Example simulation using BRouter server
=======================================

This example uses a BRouter server to query for routes. You must have
an available instance, a local server is best.

Here are `instructions on how to install it`__.

.. __: https://road-agent.readthedocs.io/en/latest/installation.html

Because of this external dependency this version is slightly more
difficult to install and run, and is more computationally
intensive. But routing is more realistic.


Jupyter interactive tutorial
____________________________

You can download the following notebook from
https://gitlab.com/rgarcia-herrera/road-agent/blob/master/doc/jupyter_tutorial.ipynb
to run a local copy and do some experiments.

Try changing speeds for modes, initial population sizes. Try other maps. Enjoy!

.. raw:: html
   :file: jupyter_tutorial_BRouter.html



