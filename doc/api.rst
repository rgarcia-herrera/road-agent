================================
Aplication Programming Interface
================================

.. automodule:: road_agent

**Agent** class
_______________

To define autonomous agents one creates instances of the Agent
class. This framework emphazises the simulation of agents moving
through streets, so methods are included for setting agents'
locations, destinations, and routes.

Points are mostly represented by LatLon points as defined by the
`LatLon23 library`_. Points in routes are just tuples of (lon, lat)
because that's what BRouter returns.

.. _`LatLon23 library`: https://pypi.org/project/LatLon23/

.. autoclass:: Agent
   :members:
   :private-members:
   :special-members:


**Router** module
_________________

The router module includes two routing classes.

NXRouter class uses the NetworkX_ library to compute shortest paths
between nodes to use as routes. It is simpler but not very robust.

BRouter class uses the requests_ library to query a BRouter_ server
for routes. BRouter is specialized on routing for bicycles and can
use different profiles for routing, including custom profiles supplied
by the user.

Here's an example of the intended use of the BRouter class::
  
   # by default the class will query a local BRouter server
   # but a different one can be supplied here
   router = Router(protocol='http',
		   host='localhost',
		   port=17777,
		   profile='fastbike')
   points = [# a source
             LatLon(Latitude(19.461332069967366),
		    Longitude(-99.09204483032227)),
	     # a destination
	     LatLon(Latitude(19.40467336236742),
		    Longitude(-99.17787551879884))]

   coarse_route = router.get_route(points=points)

   # speed in m/s
   fine_route = router.get_route(points=points,
				 speed=10)
   
   finer_route = router.get_route(points=points,
				  speed=3)
				  


.. _requests: https://pypi.org/project/requests2/
.. _NetworkX: https://networkx.github.io/
.. _BRouter: http://brouter.de/brouter/


.. automodule:: road_agent.router
   :members:
   :private-members:
   :special-members:


.. autoclass:: road_agent.router.NXRouter
   :members:
   :private-members:
   :special-members:
		
.. autoclass:: road_agent.router.BRouter
   :members:
   :private-members:
   :special-members:
