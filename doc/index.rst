.. Road Agent documentation master file, created by
   sphinx-quickstart on Wed Dec 19 15:55:06 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Road Agent
==========

This library is a framework for `agent-based modeling`_ with emphasis on
street navigation.

Create simulations of mobile agents, to be run on real-world maps.

Visit the `code repository`_.

.. image:: frames.png
    :align: center
    :alt: frames from a simulation

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   jupyter_tutorial_NXRouter
   jupyter_tutorial_BRouter   
   api

.. _`agent-based modeling`: https://en.wikipedia.org/wiki/Agent-based_model
.. _`code repository`: https://gitlab.com/rgarcia-herrera/road-agent


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
